<?php

use App\Http\Controllers\Api\Kurikulum\RombelController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// Route::post('/register', [\App\Http\Controllers\Api\AuthController::class, 'register']);
Route::post('/login', [\App\Http\Controllers\Api\AuthController::class, 'login']);

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:sanctum')->group(function () {
    /** Master
     * 001. Matapelajaran
     */
    // Route::get('/mapel/index', [App\Http\Controllers\Api\master\MapelController::class, 'index'])->name('mapel.index');
    Route::post('/mapel/index', [App\Http\Controllers\Api\master\MapelController::class, 'index'])->name('mapel.index');
    Route::post('/mapel/store', [App\Http\Controllers\Api\master\MapelController::class, 'store'])->name('mapel.store');

    Route::post('/prog/index', [App\Http\Controllers\Api\master\ProgKeahlianController::class, 'index'])->name('prog.index');
    Route::post('/prog/store', [App\Http\Controllers\Api\master\ProgKeahlianController::class, 'store'])->name('prog.store');
    
    Route::post('/bidang/index', [App\Http\Controllers\Api\master\BidangKeahlianController::class, 'index'])->name('bidang.index');
    Route::post('/bidang/store', [App\Http\Controllers\Api\master\BidangKeahlianController::class, 'store'])->name('bidang.store');
    
    Route::post('/kompt/index', [App\Http\Controllers\Api\master\KompetensiKeahlianController::class, 'index'])->name('kompt.index');
    Route::post('/kompt/store', [App\Http\Controllers\Api\master\KompetensiKeahlianController::class, 'store'])->name('kompt.store');
    
    Route::post('/spektrum/index', [App\Http\Controllers\Api\master\SpektrumController::class, 'index'])->name('spektrum.index');
    Route::post('/spektrum/store', [App\Http\Controllers\Api\master\SpektrumController::class, 'store'])->name('spektrum.store');
    Route::post('/spektrum/keahlian', [App\Http\Controllers\Api\master\SpektrumController::class, 'keahlian'])->name('spektrum.keahlian');
    Route::post('/spektrum/loadkeahlian', [App\Http\Controllers\Api\master\SpektrumController::class, 'loadkeahlian'])->name('spektrum.loadkeahlian');
    Route::post('/spektrum/loadmapelkompt', [App\Http\Controllers\Api\master\SpektrumController::class, 'loadmapelkompt'])->name('spektrum.loadmapelkompt');
    
    Route::post('/rombel/index', [App\Http\Controllers\Api\Kurikulum\RombelController::class, 'index'])->name('rombel.index');
    Route::post('/rombel/store', [App\Http\Controllers\Api\Kurikulum\RombelController::class, 'store'])->name('rombel.store');

    /** End Master */


    Route::get('/management', [App\Http\Controllers\Api\ManagementController::class, 'main_table'])->name('management.index');
    Route::post('/management', [App\Http\Controllers\Api\ManagementController::class, 'main_table'])->name('management.index');
    Route::post('/management/store', [App\Http\Controllers\Api\ManagementController::class, 'store'])->name('management.store');
    
    Route::post('/theme_app', [\App\Http\Controllers\Api\BaseController::class, 'index']);
    Route::post('/logout', [\App\Http\Controllers\Api\AuthController::class, 'logout']);
});