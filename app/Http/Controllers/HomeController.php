<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        if (view()->exists('pages/' . $request->path())) {
            return view('pages/' . $request->path());
        } else if ($request->path() == '/' || $request->path() == '/home') {
            return view('pages/home');
        }
        return abort(404);
    }

    public function pages(Request $request)
    {
        $data = $request->input();
        // session(['user_role' => 'aaaaa']);

        $getMenu = DB::table('app_role_menu')->where([
            'id' => $data['id']
        ])->get();
        $temp['content'] = '';
        $temp['plugins'] = '';
        if (view()->exists('pages/' .$getMenu[0]->menu_patch.'/default')) {
            $temp['content'] = base64_encode(View::make('pages/' .$getMenu[0]->menu_patch.'/default'));
            if(!empty($getMenu[0]->menu_patch)){
                $temp['plugins'] = base64_encode(View::make('pages/' .$getMenu[0]->menu_patch.'/plugins'));
            }
        }

        return response()->json([
            'data'  => $getMenu[0],
            'pages' => $temp,
            'auth'  => Auth::user(),
            'asd'   => Session::get('user_role')
        ]);
    }
}
