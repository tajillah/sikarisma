<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Student;
use App\Models\Dt_siswa;
use App\Models\User;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Faker\Factory as Faker;

class ManagementController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
    }

    function index(Request $request)
    {
        return response()->json([
            'access_token'  => [],
        ]);
    }

    function store(Request $request){
        $data = $request->input();
        return response()->json([
            'data'  => $data,
        ]);
    }


    function main_table(Request $request)
    {
        // return datatables()->collection(Dt_siswa::where([
        //     ['nik', '=', null],
        // ]))->toJson();

        return datatables()->query(DB::table('dt_siswa')
        ->where([
            ['nik', '=', null],
            // ['kota_lahir_name', '=', $request->input('tahun')]
        ]))->toJson();
    }
}
