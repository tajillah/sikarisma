<?php

namespace App\Http\Controllers\Api\Master;

use App\Http\Controllers\Controller;
use App\Models\Master\Spektrum;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class SpektrumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return datatables(Spektrum::query())->toJson();
    }
    public function loadkeahlian()
    {
        return datatables(DB::table('dt_kompt_spek'))->toJson();

    }
    public function loadmapelkompt()
    {
        return datatables(DB::table('dt_mapel_kompt'))->toJson();

    }

    public function keahlian()
    {
        $where['is_active'] = 'Y';
        $opr['bidang']  = DB::table('ref_bidang_ahli')->where($where)->get();
        $opr['program'] = DB::table('ref_prog_ahli')->where($where)->get();
        $opr['kompt']   = DB::table('ref_kompt_ahli')->where($where)->get();
        return response()->json($opr);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->input();
        // print_r($data);
        // exit;
        // $data['nm_mapel_raport'] = strtoupper(trim($request->nm_mapel_raport));
        // $data['nm_mapel'] = strtoupper(trim($request->nm_mapel_raport));
        // $res = Spektrum::create($data);
        $data['kd_kompt_spek'] = md5(strtotime('now'));
        $res = DB::table('dt_kompt_spek')->insert($data);
        return response()->json([
            'data'  => $data,
            'success'  => $res,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->input();
        $data['nm_mapel_raport'] = strtoupper(trim($request->nm_mapel_raport));
        $data['nm_mapel'] = strtoupper(trim($request->nm_mapel_raport));
        Spektrum::findOrFail($id)->update($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
