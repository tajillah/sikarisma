<?php

namespace App\Http\Controllers\Api\Kurikulum;

use App\Http\Controllers\Controller;
use App\Models\Kurikulum\Rombel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class RombelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return datatables(Rombel::query())->toJson();
        // return datatables()->query(DB::table('ref_mapel')
        // ->where([
        //     // ['nik', '=', null],
        //     // ['kota_lahir_name', '=', $request->input('tahun')]
        // ]))->toJson();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->input();
        $data['nm_mapel_raport'] = strtoupper(trim($request->nm_mapel_raport));
        $data['nm_mapel'] = strtoupper(trim($request->nm_mapel_raport));
        $res = Rombel::create($data);
        return response()->json([
            'data'  => $data,
            // 'success'  => $dd,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->input();
        $data['nm_mapel_raport'] = strtoupper(trim($request->nm_mapel_raport));
        $data['nm_mapel'] = strtoupper(trim($request->nm_mapel_raport));
        Rombel::findOrFail($id)->update($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
