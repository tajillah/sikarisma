<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class BaseController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
    }

    function index(Request $request)
    {
        $data = $request->input();
        $getUser = DB::table('users')->where([
            'username' => $data['username'],
        ])->get();

        session(['user_role' => $data['role']]);

        $aArrModule = DB::table('app_role_user')->where([
            'user_id' => $getUser[0]->id,
        ])->get();
        $user = $this->mainMenu($data);

        return response()->json([
            'access_token'  => $data,
            'navMenu'       => $user,
            'navModule'     => $aArrModule,
            'users'         => $getUser,
            'asd'           => Session::get('user_role')
        ]);
    }

    function mainMenu($data)
    {
        $aArrData = DB::table('app_role_menu')->orderBy('menu_number', 'asc')->where([
            'menu_parent_id' => null,
            'role_id' => $data['role']
        ])->get();

        $i = 0;
        // print_r($aArrData);
        // exit;
        foreach ($aArrData as $menu) {
            $aArrData[$i]->sub = $this->subMenu($menu->id);
            $i++;
        }
        return $aArrData;
    }

    function subMenu($main_id)
    {
        $aArrData = DB::table('app_role_menu')->where([
            'menu_parent_id' => $main_id
        ])->get();
        return empty($aArrData) ? [] : $aArrData;
    }
}
