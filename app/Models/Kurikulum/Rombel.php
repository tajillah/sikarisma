<?php

namespace App\Models\Kurikulum;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rombel extends Model
{
    protected $table = 'ref_rombel';
    protected $primaryKey = 'rombel_id';
    // public $timestamps = false;
    
    use HasFactory;
    protected $fillable = [
        'rombel_id',
        'rombel_name',
        'rombel_tahun',
        'rombel_siswa',
        'rombel_siswa_p',
        'rombel_siswa_l',
        'rombel_walikelas_id',
        'rombel_walikelas_name',
        'rombel_walikalas_nip',        
        'rombel_tingkat',        
    ];
}
