<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bidangkeahlian extends Model
{
    protected $table = 'ref_bidang_ahli';
    protected $primaryKey = 'kd_bidang';
    public $timestamps = false;
    
    use HasFactory;
    protected $fillable = [
        'kd_bidang',
        'nm_bidang',
        'is_active',
    ];
}
