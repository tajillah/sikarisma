<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mapel extends Model
{
    protected $table = 'ref_mapel';
    protected $primaryKey = 'kd_mapel';
    public $timestamps = false;
    
    use HasFactory;
    protected $fillable = [
        'kd_mapel',
        'nm_mapel_raport',
        'nm_mapel',
        'is_active',
        'tbl_color',
        'kode_mapel',
        'is_agama',
        'parent_id',
    ];
}
