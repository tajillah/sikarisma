<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Spektrum extends Model
{
    protected $table = 'ref_spektrum';
    protected $primaryKey = 'kd_spek';
    public $timestamps = false;
    
    use HasFactory;
    protected $fillable = [
        'kd_spek',
        'nm_spek',
        'tahun_mulai',
        'tahun_berakhir',
        'is_active',        
    ];
}
