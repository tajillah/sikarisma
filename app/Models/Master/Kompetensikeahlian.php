<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kompetensikeahlian extends Model
{
    protected $table = 'ref_kompt_ahli';
    protected $primaryKey = 'kd_kompt';
    public $timestamps = false;
    
    use HasFactory;
    protected $fillable = [
        'kd_kompt',
        'nm_kompt',
        'skt_kompt',
        'is_active',
        'is_ppdb',        
    ];
}
