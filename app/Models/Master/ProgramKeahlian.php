<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProgramKeahlian extends Model
{
    protected $table = 'ref_prog_ahli';
    protected $primaryKey = 'kd_prog';
    public $timestamps = false;
    
    use HasFactory;
    protected $fillable = [
        'kd_prog',
        'nm_prog',
        'is_active',
    ];
}
