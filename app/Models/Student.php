<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
class Student extends Model
{
    protected $table = 'dt_siswa';
    protected $primaryKey = 'kd_siswa';
    
    use HasFactory;
    protected $fillable = [
        'kd_siswa',
        'nis',
        'nisn',
        'nm_lkp_siswa',
        'nik',
        'jk',
        'kota_lahir_id',
        'kota_lahir_name',
        'tgl_lahir',        
    ];    
}