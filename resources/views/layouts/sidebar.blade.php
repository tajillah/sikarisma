<!-- ========== App Menu ========== -->
<div class="app-menu navbar-menu">
    <!-- LOGO -->
    <div class="navbar-brand-box">
        <!-- Dark Logo-->
        <a href="index" class="logo logo-dark">
            <span class="logo-sm">
                <img src="{{ URL::asset('assets/images/logo-sm.png') }}" alt="" height="22">
            </span>
            <span class="logo-lg">
                <img src="{{ URL::asset('assets/images/logo-dark.png') }}" alt="" height="17">
            </span>
        </a>
        <!-- Light Logo-->
        <a href="index" class="logo logo-light">
            <span class="logo-sm">
                <img src="{{ URL::asset('assets/images/logo-sm.png') }}" alt="" height="22">
            </span>
            <span class="logo-lg">
                <img src="{{ URL::asset('assets/images/logo-light.png') }}" alt="" height="17">
            </span>
        </a>
        <button type="button" class="btn btn-sm p-0 fs-20 header-item float-end btn-vertical-sm-hover" id="vertical-hover">
            <i class="ri-record-circle-line"></i>
        </button>
    </div>

    <div id="scrollbar">
        <div class="container-fluid">

            <div id="two-column-menu">
            </div>
            <ul class="navbar-nav" id="navbar-nav">
                @if (!empty(Auth::user()))
                @if (Auth::user()->role_name == 'superadminx')
                <li class="menu-title"><span>{{Auth::user()->role_name}}</span></li>
                <li class="nav-item">
                    <a class="nav-link menu-link" href="#managementSuper" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="managementSuper">
                        <i class="ri-dashboard-2-line"></i> <span>Management</span>
                    </a>
                    <div class="collapse menu-dropdown" id="managementSuper">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a href="javascript:void(0)" onclick="MainNav(this)" data-manu="" class="nav-link">Role User</a>
                            </li>
                            <li class="nav-item">
                                <a href="dashboard-analytics" class="nav-link">Role Menu</a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link menu-link" href="#masterSuper" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="masterSuper">
                        <i class="ri-dashboard-2-line"></i> <span>Master App</span>
                    </a>
                    <div class="collapse menu-dropdown" id="masterSuper">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a href="dashboard-analytics" class="nav-link">Menu</a>
                            </li>
                            <li class="nav-item">
                                <a href="dashboard-analytics" class="nav-link">Role</a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="menu-title"><span>@lang('translation.menu')</span></li>
                <li class="nav-item">
                    <a class="nav-link menu-link" href="#sidebarDashboards" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarDashboards">
                        <i class="ri-dashboard-2-line"></i> <span>@lang('translation.dashboards')</span>
                    </a>
                    <div class="collapse menu-dropdown" id="sidebarDashboards">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a href="dashboard-analytics" class="nav-link">@lang('translation.analytics')</a>
                            </li>
                        </ul>
                    </div>
                </li> <!-- end Dashboard Menu -->
                <li class="nav-item">
                    <a class="nav-link menu-link" href="widgets">
                        <i class="ri-honour-line"></i> <span>@lang('translation.widgets')</span>
                    </a>
                </li>
                @endif
                @endif
                <div id="app-container-menu"></div>
            </ul>
        </div>
        <!-- Sidebar -->
    </div>
    <div class="sidebar-background"></div>
</div>
<!-- Left Sidebar End -->
<!-- Vertical Overlay-->
<div class="vertical-overlay"></div>