<script>
    var spek_id = '';
    $(() => {
        $('#page-detail').hide();
        mainTable()
        combo()
    })

    function combo() {
        // comboKeahlian
        SERVE.ajax({
            url: "{{ route('spektrum.keahlian') }}",
            data: {
                'da': 'mm'
            },
            success: function(res) {
                $.each(res.bidang, (i, v) => {
                    $(`[data-select="bidang"]`).append(`<option value="${v['kd_bidang']}">${v['nm_bidang']}</option>`)
                })

                $.each(res.program, (i, v) => {
                    $(`[data-select="program"]`).append(`<option value="${v['kd_prog']}">${v['nm_prog']}</option>`)
                })

                $.each(res.kompt, (i, v) => {
                    $(`[data-select="kompt"]`).append(`<option value="${v['kd_kompt']}">${v['nm_kompt']}</option>`)
                })
            }
        })
    }

    // $('#search_table')

    function saveIt(formName) {
        var formData = new FormData($('#' + formName)[0]);

        SERVE.ajax({
            url: "{{ route('spektrum.store') }}",
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function(res) {
                if (res['success']) {
                    $('#modal-kompt').modal('hide');
                }
            }
        })
    }

    function newDataKompt() {
        $('#page-modal-title').html('<b>Tambah Kompetensi Spektrum<b>');
        $('#modal-kompt').modal('show');
    }

    function editData(el) {
        var data = $(el).data();
        $('#page-modal-title').html(`<b>Update Program Keahlian<b>`);
        $('#modal-main').modal('show');
    }

    function goBack() {
        $('#page-main').show();
        $('#page-detail').hide();
    }

    function goDetail(el) {
        var data = $(el).data();
        spek_id = data['id'];
        $('.nm_spek').val(data['nama'])

        $('#page-main').hide();
        $('#page-detail').show();
        loadKeahlian()
        // loadMapel()
    }

    function loadKeahlian() {
        SERVE.createTable({
            el: 'table-kompt',
            url: "{{ route('spektrum.loadkeahlian') }}",
            data: {
                spek_id: spek_id
            },
            columnDefs: [{
                    targets: 1,
                    data: 'kompt_name',
                    render: function(data, type, full, meta) {
                        return full['kompt_name']
                    }
                },
                {
                    targets: 2,
                    data: 'bidang_name',
                    render: function(data, type, full, meta) {
                        return full['bidang_name']
                    }
                },
                // {
                //     targets: 3,
                //     data: 'is_active',
                //     render: function(data, type, full, meta) {
                //         status = '<span class="badge badge-soft-primary badge-border">Enabled</span>';
                //         if (full['is_active'] == 'N') {
                //             status = '<span class="badge badge-soft-danger badge-border">Disabled</span>';
                //         }
                //         return status;
                //     }
                // },
                {
                    targets: 3,
                    orderable: false,
                    render: function(data, type, full, meta) {
                        return `<button class="btn btn-outline-primary btn-sm d-none" data-id="${full['kd_kompt_spek']}"><i class="las la-edit"></i>asd</button>`
                    }
                }
            ]
        })
    }

    function loadMapel() {
        SERVE.createTable({
            el: 'table-mapel',
            url: "{{ route('spektrum.loadmapelkompt') }}",
            data: {
                spek_id: spek_id
            },
            columnDefs: [{
                    targets: 1,
                    data: 'nm_mapel',
                    render: function(data, type, full, meta) {
                        return full['nm_mapel']
                    }
                },
                {
                    targets: 2,
                    orderable: false,
                    render: function(data, type, full, meta) {
                        return `<button class="btn btn-outline-primary btn-sm" data-id="${full['kd_mapel_kompt']}"><i class="las la-edit"></i>asd</button>`
                    }
                }
            ]
        })
    }

    function mainTable() {
        SERVE.createTable({
            url: "{{ route('spektrum.index') }}",
            // data: {
            //     tahun: 'SINGKAWANG'
            // },
            // searching:true,
            columnDefs: [{
                    targets: 1,
                    data: 'nm_spek',
                    render: function(data, type, full, meta) {
                        return full['nm_spek']
                    }
                },
                {
                    targets: 2,
                    data: 'is_active',
                    render: function(data, type, full, meta) {
                        status = '<span class="badge badge-soft-primary badge-border">Enabled</span>';
                        if (full['is_active'] == 'N') {
                            status = '<span class="badge badge-soft-danger badge-border">Disabled</span>';
                        }
                        return status;
                    }
                },
                {
                    targets: 3,
                    orderable: false,
                    render: function(data, type, full, meta) {
                        return `
                        <button class="btn btn-outline-primary btn-sm d-none" data-id="${full['kd_spek']}" data-nama="${full['nm_spek']}" onclick="editData(this)"><i class="las la-edit"></i></button>
                        <button class="btn btn-outline-primary btn-sm" data-id="${full['kd_spek']}" data-nama="${full['nm_spek']}" onclick="goDetail(this)"><i class="ri-eye-fill align-bottom"></i></button>
                        `
                    }
                }
            ]
        })
    }
</script>