<div class="row" id="page-main">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header d-flex align-items-center border-0">
                <h5 class="card-title mb-0 flex-grow-1">Daftar Spektrum</h5>
                <div class="flex-shrink-0">
                    <div class="flax-shrink-0 hstack gap-2">
                        <button class="btn btn-outline-warning btn-sm" onclick="reloadTable('maintable')"><i class="las la-redo-alt"></i></button>
                        <!-- <button class="btn btn-outline-primary btn-sm" onclick="newData()"><i class="las la-plus"></i></button> -->
                    </div>
                </div>
            </div>
            <!-- <div class="card-body">
                <input type="text" class="form-control" id="search-table" placeholder="Enter your firstname">
            </div> -->
            <div class="card-body">
                <div class="scrollbar table-card">
                    <table class="table align-middle table-nowrap" id="maintable">
                        <thead class="table-light text-muted">
                            <tr>
                                <th scope="col" style="width: 10px;">No</th>
                                <th>Name</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
        <!--end card-->
    </div>
    <!--end col-->
</div>

<div class="row" id="page-detail">
    <div class="row mb-3">
        <div class="col-sm-auto">
            <button class="btn btn-danger" onclick="goBack()"><i class="las la-chevron-left"></i> Kembali</button>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card border card-border-light">
            <div class="card-header">
                <h6 class="card-title mb-0">Detail Spektrum</h6>
            </div>
            <div class="card-body">
                <form class="tablelist-form" autocomplete="off">
                    <div class="row g-3">
                        <div class="col-lg-12">
                            <div>
                                <label class="form-label">Spektrum</label>
                                <input type="text" class="form-control nm_spek" disabled>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!--end card-->
    </div>
    <!--end col-->
    <div class="col-xl-8 col-lg-8">
        <div>
            <div class="card">
                <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col">
                            <ul class="nav nav-tabs-custom card-header-tabs border-bottom-0" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link text-body active fw-semibold" onclick="loadKeahlian()" data-bs-toggle="tab" href="#productnav-all" role="tab">
                                        Kompetensi <span class="badge badge-soft-danger align-middle rounded-pill ms-1">12</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-body fw-semibold" data-bs-toggle="tab" onclick="loadMapel()" href="#productnav-published" role="tab">
                                        Matapelajaran <span class="badge badge-soft-danger align-middle rounded-pill ms-1">5</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-auto">
                            <div id="selection-element">
                                <div class="my-n1 d-flex align-items-center text-muted">
                                    Select <div id="select-content" class="text-body fw-semibold px-1"></div> Result <button type="button" class="btn btn-link link-danger p-0 ms-3" data-bs-toggle="modal" data-bs-target="#removeItemModal">Remove</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end card header -->
                <div class="card-body">

                    <div class="tab-content text-muted">
                        <div class="tab-pane active pb-4" id="productnav-all" role="tabpanel">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="row">
                                        <div class="col-sm-auto m-0 p-0">
                                            <div class="d-flex justify-content-sm-end">
                                                <div class="search-box">
                                                    <input type="text" class="form-control form-control-sm" id="searchProductList" placeholder="Search Products...">
                                                    <i class="ri-search-line search-icon"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm">
                                            <div class="d-flex justify-content-end bd-highlight">
                                                <button type="button" onclick="newDataKompt()" class="btn btn-sm btn-primary"><i class="las la-plus-circle"></i>Tambah</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 p-0 m-0 my-2">
                                    <!-- <div id="table-product-list-all" class="table-card gridjs-border-none"></div> -->
                                    <table class="table align-middle" id="table-kompt">
                                        <thead class="table-light text-muted">
                                            <tr>
                                                <th scope="col" style="width: 10px;">No</th>
                                                <th>Kompetensi</th>
                                                <th>Bidang/Program</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- end tab pane -->

                        <div class="tab-pane pb-4" id="productnav-published" role="tabpanel">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="row">
                                        <div class="col-sm-auto m-0 p-0">
                                            <div class="d-flex justify-content-sm-end">
                                                <div class="search-box">
                                                    <input type="text" class="form-control form-control-sm" id="searchProductList" placeholder="Search Products...">
                                                    <i class="ri-search-line search-icon"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm">
                                            <div class="d-flex justify-content-end bd-highlight">
                                                <button type="button" class="btn btn-sm btn-primary"><i class="las la-plus-circle"></i>Tambah</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 p-0 m-0 my-2">
                                    <!-- <div id="table-product-list-all" class="table-card gridjs-border-none"></div> -->
                                    <table class="table align-middle" id="table-mapel">
                                        <thead class="table-light text-muted">
                                            <tr>
                                                <th scope="col" style="width: 10px;">No</th>
                                                <th>Matapelajaran</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                            <!-- <div id="table-product-list-published" class="table-card gridjs-border-none"></div> -->
                        </div>
                        <!-- end tab pane -->
                    </div>
                    <!-- end tab content -->

                </div>
                <!-- end card body -->
            </div>
            <!-- end card -->
        </div>
    </div>
</div>

<!-- Varying modal content -->
<form id="form-kompt" class="mt-3" action="javascript:saveIt('form-kompt')" method="post">
    <div class="modal fade" id="modal-kompt" tabindex="-1" aria-labelledby="modal-mainLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-mainLabel"><span id="page-modal-title"></span></h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">


                    <div class="row">
                        <div class="col-12">
                            <div class="mb-3">
                                <label for="compnayNameinput" class="form-label">Bidang Keahlian</label>
                                <select class="form-control" data-select="bidang" name="bidang_id" id="bidang_id"></select>
                            </div>
                        </div><!--end col-->
                        <div class="col-12">
                            <div class="mb-3">
                                <label for="compnayNameinput" class="form-label">Program Keahllian</label>
                                <select class="form-control" data-select="program" name="prog_id" id="prog_id"></select>
                                <!-- <input type="text" class="form-control" placeholder="Enter company name" id="compnayNameinput"> -->
                            </div>
                        </div><!--end col-->
                        <div class="col-12">
                            <div class="mb-3">
                                <label for="compnayNameinput" class="form-label">Kompetensi Keahllian</label>
                                <select class="form-control" data-select="kompt" name="kompt_id" id="kompt_id"></select>
                            </div>
                        </div><!--end col-->
                    </div><!--end row-->


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Send message</button>
                </div>
            </div>
        </div>
    </div>
</form>