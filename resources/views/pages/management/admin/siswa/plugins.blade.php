<script>
    $(() => {
        SERVE.coretable({
            url: "{{ route('management.index') }}",
            // data: {
            //     tahun: 'SINGKAWANG'
            // },
            columnDefs: [
                {
                    targets: 1,
                    data: 'nik',
                    render: function(data, type, full, meta) {
                        return full['nik']
                    }
                },
                {
                    targets: 2,
                    data: 'nm_lkp_siswa',
                    render: function(data, type, full, meta) {
                        return full['nm_lkp_siswa']
                    }
                },
                {
                    targets: 3,
                    data: 'jk',
                    render: function(data, type, full, meta) {
                        return full['jk']
                    }
                },
                {
                    targets: 4,
                    data: 'kota_lahir_name',
                    render: function(data, type, full, meta) {
                        return full['kota_lahir_name']
                    }
                },
                {
                    targets: 5,
                    orderable: false,
                    render: function(data, type, full, meta) {
                        return 'sad'
                    }
                }
            ]
        })
        console.log('welcome')
        // $('#maintable').DataTable({
        //     lengthChange: false,
        //     searching:false
        // });
        // $('#maintable').DataTable({
        //     searching: false,
        //     processing: true,
        //     serverSide: true,
        //     bLengthChange: false,
        //     oLanguage: {
        //         sInfo: "Showing _START_ to _END_ of _TOTAL_ entries",
        //         sInfoEmpty: "Showing 0 to 0 of 0 entries"
        //     },
        //     // BeforeSend: {
        //     //     'Authorization': "Bearer " + getCookie("access_token")
        //     // },
        //     ajax: {
        //         type: "POST",
        //         beforeSend: function(xhr) {
        //             xhr.setRequestHeader(
        //                 "Authorization",
        //                 "Bearer " + getCookie("access_token")
        //             );
        //         },
        //         dataSrc: "data",
        //         url: "{{ route('management.index') }}",
        //     },
        //     columns: [{
        //             data: 'kd_siswa',
        //             name: 'kd_siswa'
        //         },
        //         {
        //             data: 'nm_lkp_siswa',
        //             name: 'nm_lkp_siswa'
        //         },
        //         {
        //             data: 'jk',
        //             name: 'jk'
        //         },
        //         {
        //             data: 'nik',
        //             name: 'nik'
        //         },
        //         {
        //             data: 'tgl_lahir',
        //             name: 'tgl_lahir'
        //         },
        //         {
        //             data: 'nis',
        //             name: 'nis',
        //             orderable: false,
        //             searchable: false
        //         },
        //     ]
        // });
    })

    function newData(){
        $('#modal-main').modal('show')
    }
</script>