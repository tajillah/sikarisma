<script>
    $(() => {
        mainTable()
    })


    function saveIt(formName) {
        var formData = new FormData($('#'+formName)[0]);
        SERVE.ajax({
            url: "{{ route('management.store') }}",
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function(res) {
                console.log(res)
            }
        })
    }

    function newData() {
        $('#page-modal-title').html('<b>Tambah Data Pegawai<b>');
        $('#modal-main').modal('show');
    }

    function editData(el) {
        var data = $(el).data();
        $('#page-modal-title').html(`<b>Update Data : ${data['name']}<b>`);
        $('#modal-main').modal('show');
    }


    function mainTable() {
        SERVE.createTable({
            url: "{{ route('management.index') }}",
            // data: {
            //     tahun: 'SINGKAWANG'
            // },
            columnDefs: [{
                    targets: 1,
                    data: 'nik',
                    render: function(data, type, full, meta) {
                        return full['nik']
                    }
                },
                {
                    targets: 2,
                    data: 'nm_lkp_siswa',
                    render: function(data, type, full, meta) {
                        return full['nm_lkp_siswa']
                    }
                },
                {
                    targets: 3,
                    data: 'jk',
                    render: function(data, type, full, meta) {
                        return full['jk']
                    }
                },
                {
                    targets: 4,
                    data: 'kota_lahir_name',
                    render: function(data, type, full, meta) {
                        return full['kota_lahir_name']
                    }
                },
                {
                    targets: 5,
                    orderable: false,
                    render: function(data, type, full, meta) {
                        return `<button class="btn btn-primary btn-sm" data-id="${full['kd_siswa']}" data-name="${full['nm_lkp_siswa']}" onclick="editData(this)">Edit</button>`
                    }
                }
            ]
        })
    }
</script>