<div class="row" id="contactList">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header d-flex align-items-center border-0">
                <h5 class="card-title mb-0 flex-grow-1">Daftar Pegawai</h5>
                <div class="flex-shrink-0">
                    <div class="flax-shrink-0 hstack gap-2">
                        <button class="btn btn-warning" onclick="reloadTable('maintable')">Refres</button>
                        <button class="btn btn-primary" onclick="newData()">New</button>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="scrollbar table-card">
                    <table class="table align-middle table-nowrap" id="maintable">
                        <thead class="table-light text-muted">
                            <tr>
                                <th scope="col" style="width: 10px;">No</th>
                                <th>NIS</th>
                                <th>Nama</th>
                                <th>JK</th>
                                <th>TANGGAL</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
        <!--end card-->
    </div>
    <!--end col-->
</div>

<!-- Varying modal content -->
<form id="form-data" class="mt-3" action="javascript:saveIt('form-data')" method="post">
    <div class="modal fade" id="modal-main" tabindex="-1" aria-labelledby="modal-mainLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-mainLabel"><span id="page-modal-title"></span></h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="border border-dashed border-end-0 border-start-0">
                        <div class="form-floating mt-3">
                            <input type="text" class="form-control" id="firstnamefloatingInput" name="nmLkpSiswa"  placeholder="Enter your firstname">
                            <label for="firstnamefloatingInput">Nama Lengkap Siswa</label>
                        </div>
                        <div class="form-floating mt-3">
                            <input type="text" class="form-control" id="firstnamefloatingInput" name="TglLhr"  placeholder="Enter your firstname">
                            <label for="firstnamefloatingInput">Tempat lahir</label>
                        </div>
                        <div class="form-floating mt-3">
                            <input type="text" class="form-control" id="firstnamefloatingInput" name="TmptLhr"  placeholder="Enter your firstname">
                            <label for="firstnamefloatingInput">Tanggal Lahir</label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Send message</button>
                </div>
            </div>
        </div>
    </div>
</form>