<script>
    var spek_id = '';
    $(() => {
        $('#page-detail').hide();
        mainTable()
    })

    // $('#search_table')

    function saveIt(formName) {
        var formData = new FormData($('#'+formName)[0]);
        SERVE.ajax({
            url: "{{ route('rombel.store') }}",
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function(res) {
                console.log(res)
            }
        })
    }

    function newData() {
        $('#page-modal-title').html('<b>Tambah Program Keahlian<b>');
        $('#modal-main').modal('show');
    }

    function editData(el) {
        var data = $(el).data();
        $('#page-modal-title').html(`<b>Update Program Keahlian<b>`);
        $('#modal-main').modal('show');
    }

    // $('#search-table').on('blur',()=>{
    //     console.log(this)
    // })
    // $('[data-search_table="true"]').onblur(function() {
	// 	console.log(this)
	// });

    function mainTable() {
        SERVE.createTable({
            url: "{{ route('rombel.index') }}",
            // data: {
            //     tahun: 'SINGKAWANG'
            // },
            // searching:true,
            columnDefs: [{
                    targets: 1,
                    data: 'rombel_name',
                    render: function(data, type, full, meta) {
                        return full['rombel_name']
                    }
                },
                {
                    targets: 2,
                    data: 'rombel_tingkat',
                    render: function(data, type, full, meta) {
                        return full['rombel_tingkat'];
                    }
                },
                {
                    targets: 3,
                    data: 'rombel_walikelas_name',
                    render: function(data, type, full, meta) {
                        return full['rombel_walikelas_name'];
                    }
                },
                // {
                //     targets: 2,
                //     data: 'is_active',
                //     render: function(data, type, full, meta) {
                //         status = '<span class="badge badge-soft-primary badge-border">Enabled</span>';
                //         if(full['is_active'] == 'N'){
                //             status = '<span class="badge badge-soft-danger badge-border">Disabled</span>';
                //         }
                //         return status;
                //     }
                // },
                {
                    targets: 4,
                    orderable: false,
                    render: function(data, type, full, meta) {
                        return `
                        <button class="btn btn-outline-primary btn-sm" data-id="${full['kd_spek']}" data-nama="${full['nm_spek']}" onclick="editData(this)"><i class="las la-edit"></i></button>
                        <button class="btn btn-outline-primary btn-sm d-none" data-id="${full['kd_spek']}" data-nama="${full['nm_spek']}" onclick="goDetail(this)"><i class="ri-eye-fill align-bottom"></i></button>
                        `
                    }
                }
            ]
        })
    }

    function goBack(){
        $('#page-main').show();
        $('#page-detail').hide();
    }

    function goDetail(el){
        var data = $(el).data();
        spek_id = data['id'];
        $('.nm_spek').val(data['nama'])
        
        $('#page-main').hide();
        $('#page-detail').show();
    }
</script>