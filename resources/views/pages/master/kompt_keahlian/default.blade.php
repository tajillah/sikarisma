<div class="row" id="contactList">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header d-flex align-items-center border-0">
                <h5 class="card-title mb-0 flex-grow-1">Daftar Kompetsni Keahlian</h5>
                <div class="flex-shrink-0">
                    <div class="flax-shrink-0 hstack gap-2">
                        <button class="btn btn-outline-warning btn-sm" onclick="reloadTable('maintable')"><i class="las la-redo-alt"></i></button>
                        <button class="btn btn-outline-primary btn-sm" onclick="newData()"><i class="las la-plus"></i></button>
                    </div>
                </div>
            </div>
            <!-- <div class="card-body">
                <input type="text" class="form-control" id="search-table" placeholder="Enter your firstname">
            </div> -->
            <div class="card-body">
                <div class="scrollbar table-card">
                    <table class="table align-middle table-nowrap" id="maintable">
                        <thead class="table-light text-muted">
                            <tr>
                                <th scope="col" style="width: 10px;">No</th>
                                <th>Kompetensi</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
        <!--end card-->
    </div>
    <!--end col-->
</div>

<!-- Varying modal content -->
<form id="form-data" class="mt-3" action="javascript:saveIt('form-data')" method="post">
    <div class="modal fade" id="modal-main" tabindex="-1" aria-labelledby="modal-mainLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-mainLabel"><span id="page-modal-title"></span></h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="border border-dashed border-end-0 border-start-0">
                        <div class="form-floating mt-3">
                            <input type="text" class="form-control" id="kode_mapel" name="kode_mapel">
                            <label for="kode_mapel">KODE</label>
                        </div>
                        <div class="form-floating mt-3">
                            <input type="text" class="form-control" id="nm_mapel_raport" name="nm_mapel_raport">
                            <label for="nm_mapel_raport">Matapelajaran</label>
                        </div>
                        <div class="form-floating mt-3">
                            <input type="color" class="form-control" id="tbl_color" name="tbl_color">
                            <label for="tbl_color">Color</label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Send message</button>
                </div>
            </div>
        </div>
    </div>
</form>