<script>
    $(() => {
        mainTable()
    })

    // $('#search_table')

    function saveIt(formName) {
        var formData = new FormData($('#'+formName)[0]);
        SERVE.ajax({
            url: "{{ route('mapel.store') }}",
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function(res) {
                console.log(res)
            }
        })
    }

    function newData() {
        $('#page-modal-title').html('<b>Tambah Data Pegawai<b>');
        $('#modal-main').modal('show');
    }

    function editData(el) {
        var data = $(el).data();
        $('#page-modal-title').html(`<b>Update Data : ${data['name']}<b>`);
        $('#modal-main').modal('show');
    }

    // $('#search-table').on('blur',()=>{
    //     console.log(this)
    // })
    // $('[data-search_table="true"]').onblur(function() {
	// 	console.log(this)
	// });

    function mainTable() {
        SERVE.createTable({
            url: "{{ route('mapel.index') }}",
            // data: {
            //     tahun: 'SINGKAWANG'
            // },
            // searching:true,
            columnDefs: [{
                    targets: 1,
                    data: 'kode_mapel',
                    render: function(data, type, full, meta) {
                        return full['kode_mapel']
                    }
                },
                {
                    targets: 2,
                    data: 'nm_mapel_raport',
                    render: function(data, type, full, meta) {
                        return full['nm_mapel_raport']
                    }
                },
                {
                    targets: 3,
                    data: 'tbl_color',
                    render: function(data, type, full, meta) {
                        return `<span class="p-2" style="background-color:${full['tbl_color']}"></span> ${full['tbl_color']}`
                    }
                },
                {
                    targets: 4,
                    data: 'is_active',
                    render: function(data, type, full, meta) {
                        status = '<span class="badge badge-soft-primary badge-border">Enabled</span>';
                        if(full['is_active'] == 'N'){
                            status = '<span class="badge badge-soft-danger badge-border">Disabled</span>';
                        }
                        return status;
                    }
                },
                {
                    targets: 5,
                    orderable: false,
                    render: function(data, type, full, meta) {
                        return `<button class="btn btn-outline-primary btn-sm" data-id="${full['kd_mapel']}" data-kode="${full['kode_mapel']}" onclick="editData(this)"><i class="las la-edit"></i></button>`
                    }
                }
            ]
        })
    }
</script>