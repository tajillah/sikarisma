<script>
    $(() => {
        mainTable()
    })

    // $('#search_table')

    function saveIt(formName) {
        var formData = new FormData($('#'+formName)[0]);
        SERVE.ajax({
            url: "{{ route('prog.store') }}",
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function(res) {
                console.log(res)
            }
        })
    }

    function newData() {
        $('#page-modal-title').html('<b>Tambah Program Keahlian<b>');
        $('#modal-main').modal('show');
    }

    function editData(el) {
        var data = $(el).data();
        $('#page-modal-title').html(`<b>Update Program Keahlian<b>`);
        $('#modal-main').modal('show');
    }

    // $('#search-table').on('blur',()=>{
    //     console.log(this)
    // })
    // $('[data-search_table="true"]').onblur(function() {
	// 	console.log(this)
	// });

    function mainTable() {
        SERVE.createTable({
            url: "{{ route('prog.index') }}",
            // data: {
            //     tahun: 'SINGKAWANG'
            // },
            // searching:true,
            columnDefs: [{
                    targets: 1,
                    data: 'nm_prog',
                    render: function(data, type, full, meta) {
                        return full['nm_prog']
                    }
                },
                {
                    targets: 2,
                    data: 'is_active',
                    render: function(data, type, full, meta) {
                        status = '<span class="badge badge-soft-primary badge-border">Enabled</span>';
                        if(full['is_active'] == 'N'){
                            status = '<span class="badge badge-soft-danger badge-border">Disabled</span>';
                        }
                        return status;
                    }
                },
                {
                    targets: 3,
                    orderable: false,
                    render: function(data, type, full, meta) {
                        return `<button class="btn btn-outline-primary btn-sm" data-id="${full['kd_prog']}" data-nama="${full['nm_prog']}" onclick="editData(this)"><i class="las la-edit"></i></button>`
                    }
                }
            ]
        })
    }
</script>