@extends('layouts.master')
@section('title') Home @endsection
@section('css')
<!--datatable css-->
<link href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap5.min.css" rel="stylesheet" type="text/css" />
<!--datatable responsive css-->
<link href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.datatables.net/buttons/2.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
@endsection
@section('content')
@component('components.breadcrumb')
@slot('li_1') Dashboards @endslot
@slot('title')Dashboards @endslot
@endcomponent
<!-- {{env('APP_URL', '3306')}} -->
<!-- <button type="button" class="btn btn-primary" onclick="getAppThemes()">asdadssad</button> -->
<div id="app-panel-content">
    <!-- {{Auth::user()}} -->
</div>
@endsection
@section('script')
<script>
    $(document).ready(function() {
        setTimeout(() => {
            getAppThemes()
        }, 100);
        // console.log(BASE_URL);
        // getAppConfig()
        // $('.list-group a[href="/{{Request::path()}}"]').addClass('active');
    });

    function goModules(params) {
        var data = $(params).data()
        console.log(data)
        setCookie("module_name", data['modules'], 3600);
        setCookie("module_id", data['id'], 3600);
        getAppThemes()
    }

    function getAppThemes() {
        var navHtml = '';
        var navModul = '';
        var subNav = '';
        var first_data = '';
        $('#app-container-menu').html('');
        SERVE.ajax({
            url: '/api/theme_app',
            data: {
                role: getCookie('module_id'),
                username: getCookie('username'),
            },
            success: function(data) {
                $.each(data['navMenu'], (i, v) => {
                    if (v.sub.length > 0) {
                        subNav = '';
                        $.each(v.sub, (ii, vv) => {
                            var datamenu = btoa(JSON.stringify(vv));
                            subNav += `
                                <li class="nav-item">
                                    <a href="javascript:void(0)" data-menu="${datamenu}" data-title="${v.menu_title}" data-sub_title="${vv.menu_title}" data-navapp="main" onclick="gotopages(this)" class="nav-link">${vv.menu_name}</a>
                                </li>
                            `;
                        })
                        navHtml += `
                        <li class="nav-item">
                            <a class="nav-link menu-link" href="#menu-${v.id}" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarDashboards">
                                <i class="ri-dashboard-2-line"></i> <span>${v.menu_name}</span>
                            </a>
                            <div class="collapse menu-dropdown" id="menu-${v.id}">
                                <ul class="nav nav-sm flex-column">
                                    ${subNav}
                                </ul>
                            </div>
                        </li> 
                        `;
                    } else {
                        var datamenu = btoa(JSON.stringify(v));
                        navHtml += `
                            <li class="nav-item">
                                <a class="nav-link menu-link" href="javascript:void(0)" data-menu="${datamenu}" data-title="${v.menu_title}" data-sub_title="${v.menu_title}" data-navapp="main" onclick="gotopages(this)">
                                    <i class="ri-honour-line"></i> <span>${v.menu_name}</span>
                                </a>
                            </li>
                        `;
                    }
                    first_data = first_data == '' ? `<a class="nav-link menu-link" href="javascript:void(0)" data-menu="${datamenu}" data-title="${v.menu_title}" data->sub_title="${v.menu_title}" data-navapp="main"></a>` : first_data;
                })
                iNumb = 1;
                mm = '';
                split = data['navModule'].length / 2;
                $.each(data['navModule'], (i, v) => {
                    mm += `
                            <div class="col">
                                <a class="dropdown-icon-item" href="javascript:void(0)" data-id="${v.role_id}" data-modules="${v.role_name}" onclick="goModules(this)">
                                    <img src="{{ URL::asset('assets/images/brands/github.png') }}" alt="${v.role_name}">
                                    <span>${String(v.role_name).toUpperCase()}</span>
                                </a>
                            </div>`;
                    if (iNumb > 2) {
                        navModul += `<div class="row g-0">${mm}</div>`;
                        mm = '';
                        iNumb = 1;
                    } else {
                        iNumb++;
                    }
                })
                $('#app-container-menu').html(navHtml);
                $('#app-panel-module').html(navModul);
                setTimeout(() => {
                    gotopages(first_data)
                }, 200);
            }
        })
    }

    function gotopages(params) {
        var data = $(params).data();

        var menu = JSON.parse(atob(data['menu']))
        console.log(menu)
        $('#app-panel-content').html('');
        SERVE.ajax({
            url: '/go_pages',
            data: menu,
            success: function(res) {
                $('#pages-title').html(String(res['data']['menu_title']).toUpperCase());
                $('#breadcrumb-title').html(data['title']);
                $('#breadcrumb-sub_title').html(menu['menu_breadcrumb']);
                $('#app-panel-content').html(atob(res['pages']['content']));
                $('#app-panel-plugins').html(atob(res['pages']['plugins']));
            }
        })
    }
</script>
<div id="app-panel-plugins">

</div>
@endsection