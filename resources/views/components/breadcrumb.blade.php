<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 id="pages-title" class="mb-sm-0 font-size-18">{{ $title }}</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);"><span id="breadcrumb-title">{{ $li_1 }}</span></a></li>
                    @if(isset($title))
                        <li class="breadcrumb-item active"><span id="breadcrumb-sub_title">{{ $title }}</span></li>
                    @endif
                </ol>
            </div>

        </div>
    </div>
</div>
<!-- end page title -->
