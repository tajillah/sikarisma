Laravel Auth
Web Login
```bash
composer require laravel/ui
```
Once the `laravel/ui` package has been installed, you may install the frontend scaffolding using the `ui` Artisan command:
```bash
// Generate basic scaffolding...
php artisan ui bootstrap
php artisan ui vue
php artisan ui react

// Generate login / registration scaffolding...
php artisan ui bootstrap --auth
php artisan ui vue --auth
php artisan ui react --auth
```
API Login
```bash
composer require laravel/sanctum
```